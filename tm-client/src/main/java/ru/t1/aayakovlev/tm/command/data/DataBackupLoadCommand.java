package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.DataBackupLoadRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;


public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-backup-load";

    @NotNull
    private static final String DESCRIPTION = "Load backup data.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        getDomainEndpoint().backupLoad(new DataBackupLoadRequest(getToken()));
    }

}
