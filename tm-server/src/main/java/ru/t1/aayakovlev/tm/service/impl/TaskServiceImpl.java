package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskServiceImpl implements TaskService {

    @NotNull
    private final ConnectionService connectionService;

    public TaskServiceImpl(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    static {
        // TODO: 15.11.2022 вызов создания таблицы из репозитория
    }

    @NotNull
    private String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        if (comparator == NameComparator.INSTANCE) return "name";
        return "id";
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final Task task = findById(userId, id);
            task.setStatus(status);
            @NotNull final TaskRepository repository = session.getMapper(TaskRepository.class);
            repository.update(task);
            session.commit();
            return task;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        return save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task(name, description);
        task.setUserId(userId);
        return save(task);
    }

    @NotNull
    public Task save(@NotNull final Task task) {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull TaskRepository repository = session.getMapper(TaskRepository.class);
            repository.save(task);
            session.commit();
            return task;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final TaskRepository repository = session.getMapper(TaskRepository.class);
            @Nullable final List<Task> tasks = repository.findAllByProjectId(userId, projectId);
            if (tasks == null) return new ArrayList<>();
            return tasks;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final Task task = findById(userId, id);
            task.setName(name);
            task.setDescription(description);
            @NotNull final TaskRepository repository = session.getMapper(TaskRepository.class);
            repository.update(task);
            session.commit();
            return task;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable final List<Task> tasks) {
        if (tasks == null || tasks.size() == 0) return;
        for (@NotNull final Task task: tasks) {
            save(task);
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final TaskRepository repository = session.getMapper(TaskRepository.class);
            repository.clear();
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final TaskRepository repository = session.getMapper(TaskRepository.class);
            repository.clear(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public int count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull TaskRepository repository = session.getMapper(TaskRepository.class);
            return repository.count(userId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull TaskRepository repository = session.getMapper(TaskRepository.class);
            @Nullable final List<Task> tasks = repository.findAll(userId);
            if (tasks == null) return new ArrayList<>();
            return tasks;
        }
    }

    @Override
    public @NotNull List<Task> findAll() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull TaskRepository repository = session.getMapper(TaskRepository.class);
            @Nullable final List<Task> tasks = repository.findAll();
            if (tasks == null) return new ArrayList<>();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull TaskRepository repository = session.getMapper(TaskRepository.class);
            @Nullable final List<Task> tasks = repository.findAll(userId, getSortType(sort.getComparator()));
            if (tasks == null) return new ArrayList<>();
            return tasks;
        }
    }

    @NotNull
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull TaskRepository repository = session.getMapper(TaskRepository.class);
            @Nullable final Task task = repository.findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull TaskRepository repository = session.getMapper(TaskRepository.class);
            repository.removeById(userId, id);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
