package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.Domain;

public interface DomainService {

    @NotNull
    Domain getDomain();

    void setDomain(@Nullable final Domain domain);

    void backupLoad();

    void backupSave();

    void base64Load();

    void base64Save();

    void binaryLoad();

    void binarySave();

    void jsonLoadFXml();

    void jsonLoadJaxB();

    void jsonSaveFXml();

    void jsonSaveJaxB();

    void xmlLoadFXml();

    void xmlLoadJaxB();

    void xmlSaveFXml();

    void xmlSaveJaxB();

    void yamlLoad();

    void yamlSave();

}
