package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.SessionRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import javax.sql.DataSource;

public final class ConnectionServiceImpl implements ConnectionService {

    @NotNull
    private final DatabaseProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionServiceImpl(@NotNull final DatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @SneakyThrows
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseURL();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(ProjectRepository.class);
        configuration.addMapper(TaskRepository.class);
        configuration.addMapper(UserRepository.class);
        configuration.addMapper(SessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
