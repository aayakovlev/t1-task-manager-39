package ru.t1.aayakovlev.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Session;

import java.util.List;

public interface SessionRepository {

    @Delete("DELETE FROM tm_session")
    void clear();

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") final String userId);

    @Select("SELECT COUNT(id) FROM tm_session WHERE user_id = #{userId}")
    int count(@NotNull @Param("userId") final String userId);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = { @Result(property = "userId", column = "user_id")})
    List<Session> findAll(@NotNull @Param("userId") final String userId);

    @Nullable
    @Results(value = { @Result(property = "userId", column = "user_id") })
    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    List<Session> findAll(@NotNull @Param("userId") final String userId, @NotNull @Param("order") final String order);

    @Nullable
    @Results(value = { @Result(property = "userId", column = "user_id") })
    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    Session findById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Nullable
    @Select("SELECT FROM tm_session WHERE user_id = #{userId} LIMIT BY 1 OFFSET #{index}")
    Session findByIndex(@NotNull @Param("userId") final String userId, @Param("index") final int index);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Insert("INSERT INTO tm_session (id, date, role, user_id) VALUES (#{id}, #{date}, #{role}, #{userId})")
    void save(@NotNull final Session session);

    @Update("UPDATE tm_session SET date = #{date}, role = #{role}, user_id = #{userId} WHERE id = #{id}")
    void update(@NotNull final Session session);

}
