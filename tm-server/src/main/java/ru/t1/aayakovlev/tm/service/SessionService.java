package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.Session;

import java.util.List;

public interface SessionService {

    void clear();

    void clear(@Nullable final String userId) throws UserIdEmptyException;

    int count(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<Session> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException;

    @NotNull
    List<Session> findAll(@Nullable final String userId) throws UserIdEmptyException;

    @NotNull
    Session findById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    Session save(@NotNull final Session model);

    boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

}
