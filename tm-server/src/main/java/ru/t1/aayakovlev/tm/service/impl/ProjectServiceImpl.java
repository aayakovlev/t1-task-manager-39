package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.util.*;

public final class ProjectServiceImpl implements ProjectService {

    @NotNull
    private final ConnectionService connectionService;

    public ProjectServiceImpl(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    static {
        // TODO: 15.11.2022 вызов создания таблицы из репозитория
    }

    @NotNull
    private String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        if (comparator == NameComparator.INSTANCE) return "name";
        return "id";
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Project project = findById(userId, id);
        project.setStatus(status);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.update(project);
            session.commit();
            return project;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return save(project);
    }


    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project(name, description);
        project.setUserId(userId);
        return save(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.update(project);
            session.commit();
            return project;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable final List<Project> projects) {
        clear();
        if (projects == null || projects.size() == 0) return;
        for (@NotNull final Project project : projects) {
            save(project);
        }
    }

    @NotNull
    public Project save(@NotNull final Project project) {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.save(project);
            session.commit();
            return project;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.clear(userId);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public int count(@Nullable final String userId) throws AbstractException{
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            return repository.count(userId);
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.clear();
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            @Nullable final List<Project> projects = repository.findAll(userId, getSortType(sort.getComparator()));
            if (projects == null) return new ArrayList<>();
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            @Nullable final List<Project> projects = repository.findAll(userId);
            if (projects == null) return new ArrayList<>();
            return projects;
        }
    }

    @Override
    public @NotNull List<Project> findAll() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            @Nullable final List<Project> projects = repository.findAll();
            if (projects == null) return new ArrayList<>();
            return projects;
        }
    }

    @NotNull
    @Override
    public Project findById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            @Nullable final Project project = repository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Project project = new Project();
        project.setId(id);
        project.setUserId(userId);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull ProjectRepository repository = session.getMapper(ProjectRepository.class);
            repository.removeById(userId, id);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
