CREATE TABLE public."user"
(
    id varchar(36) NOT NULL,
    login varchar(64) NOT NULL,
    password_hash varchar(32) NOT NULL,
    email varchar(64),
    role varchar(64),
    first_name varchar(64),
    last_name varchar(64),
    middle_name varchar(64),
    locked boolean,
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE public.session
(
    id varchar(36) NOT NULL,
    date timestamp NOT NULL,
    role varchar(64),
    user_id varchar(36) NOT NULL,
    CONSTRAINT session_pkey PRIMARY KEY (id)
);

CREATE TABLE public.project
(
    id varchar(36) NOT NULL,
    name varchar(64) NOT NULL,
    description varchar(255),
    user_id varchar(36),
    status varchar(64),
    created timestamp NOT NULL,
    CONSTRAINT project_pkey PRIMARY KEY (id)
);

CREATE TABLE public.task
(
    id varchar(36) NOT NULL,
    name varchar(64) NOT NULL,
    description varchar(255),
    user_id varchar(36),
    project_id varchar(36),
    status varchar(64) NOT NULL,
    created timestamp NOT NULL,
    CONSTRAINT task_pkey PRIMARY KEY (id)
);
