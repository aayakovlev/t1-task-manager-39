package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

    @Nullable
    private List<Project> projects;

    public AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

    public AbstractProjectResponse(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

}
