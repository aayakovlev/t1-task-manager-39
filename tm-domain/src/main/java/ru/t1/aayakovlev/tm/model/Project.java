package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements WBS {

    private static final long serialVersionUID = 0;

    @NotNull
    private Date created = new Date();

    @NotNull
    private String description = "";

    @NotNull
    private String name = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    public Project(@NotNull final String name, @NotNull final String description) {
        this.setName(name);
        this.setDescription(description);
    }

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.setName(name);
        this.setStatus(status);
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
